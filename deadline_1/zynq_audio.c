/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *    
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "audio.h"
#include "oled.h"
#include "sleep.h"
#include <stdlib.h>

#include "fft.h"
#define WINDOW_NUM 8

/* debug messages */
#define DBG_AUDIO_DATA 0
#define DBG_FFT_OUT 0
#define DBG_SPECTRUM_AVERAGE 0

/* latency measurement */
#define MEASURE_TIME 0

/* implementation checking */
#define SIMULATED_INPUT 0

/* scaling and tresholding */
#define OLED_MAX_SCALE		40 	/* 40 */
#define RESCALING_FACTOR	4	/* 4 - in term of binary shift */
#define TRESHOLD_LEVEL		50	/* 50 */

#define CAP_LEVEL			(OLED_MAX_SCALE<<RESCALING_FACTOR)

//void print(char *str);

#if (SIMULATED_INPUT==1)
#include <math.h>
#endif

//#if (MEASURE_TIME==1)
#include "xtime_l.h"
#include "xil_io.h"
#include "xparameters.h"

#define timer_base 0xF8F00000
/***********************************************************
Timer Registers
************************************************************/
static volatile int *timer_counter_l=(volatile int *)(timer_base+0x200);
static volatile int *timer_counter_h=(volatile int *)(timer_base+0x204);
static volatile int *timer_ctrl=(volatile int *)(timer_base+0x208);
/***********************************************************
Function definitions
************************************************************/
void init_timer(volatile int *timer_ctrl, volatile int *timer_counter_l, volatile int *timer_counter_h){
        *timer_ctrl=0x0;
        *timer_counter_l=0x1;
        *timer_counter_h=0x0;
        DATA_SYNC;
}

void start_timer(volatile int *timer_ctrl){
        *timer_ctrl=*timer_ctrl | 0x00000001;
        DATA_SYNC;
}

void stop_timer(volatile int *timer_ctrl){
        *timer_ctrl=*timer_ctrl & 0xFFFFFFFE;
        DATA_SYNC;
}
//#endif

/* ************************************************************************
 * Steps:
 * -----
 * 1. Do these:
 *    1.1. Take 128 of audio data.
 *    1.2. FFT
 * 2. Do averaging of those 8 windows (if dataset is less than 8, assume rest are 0).
 * 3. 
 * ************************************************************************ */
int main()
{

	Xint16 audio_data[128];
	int i, j;
	u8 *oled_equalizer_buf=(u8 *)malloc(128*sizeof(u8));

	/* -------- SIGNAL VARIABLES -------- */
	float spectrum_data[WINDOW_NUM][128];	/* raw signal data */
	float spectrum_averg[128];				/* averaged signal data over WINDOW_NUM */
	float spectrum_noise[128];				/* noise profile, to be substracted to spectrum */
	u8 spectrum_last_idx;					/* hold the current location of RING BUFFER of WINDOW_NUM */
	float fft_data[128*2];					/* output of FFT, real + imaginary */

	/* -------- LED and HW related -------- */
	Xil_Out32(OLED_BASE_ADDR,0xff);
	OLED_Init();			//oled init
	IicConfig(XPAR_XIICPS_0_DEVICE_ID);
	AudioPllConfig(); //enable core clock for ADAU1761
	AudioConfigure();

	xil_printf("ADAU1761 configured\n\r");




	/*
	 * perform continuous read and writes from the codec that result in a loopback
	 * from Line in to Line out
	 */

	/* being paranoid in case .bss section is not really zeroed */
	spectrum_last_idx = 0;
	// clear spectrum_data[][] as well; this part is not counted for latency :)
	for(i=0;i<WINDOW_NUM;i++){
		for(j=0;j<128;j++){
			spectrum_data[i][j] = 0;
		}
	}


	/* ######## collect the NOISE PROFILE over first 5 second ######################################### */

	int tCount = 0;
	xil_printf("Collecting noise signal: ");
	while(tCount<5) /* 5 stand for 5 second */
	{
		init_timer(timer_ctrl, timer_counter_l, timer_counter_h);
		start_timer(timer_ctrl);
		xil_printf("%d,", tCount);

		while( ((*timer_counter_l)) < (1*1000*1000*333) ) /* counter value for 1 sec period */
		{

			//get data
			get_audio(audio_data);

			//being paranoid...
			for(i=0;i<WINDOW_NUM;i++){
				for(j=0;j<128;j++){
					spectrum_data[i][j] = 0;
				}
			}
			for(i=0; i<(128*2); i+=2){
				fft_data[i] = 0;
				fft_data[i+1] = 0; //imag. part
			}

			/* #1. Take 128 audio data */

			for(i=0;i<128;i++)
			{
				spectrum_data[spectrum_last_idx][i]=(audio_data[i])+10; //ensure no negative signal
			}

			/* #2. Do FFT, store in cyclic buffer of 8-size. */
			// construct for FFT data input
			for(i=0; i<(128*2); i+=2){
				fft_data[i] = spectrum_data[spectrum_last_idx][i]; //real part
				//only take positive value, why?
				//if (fft_data[i] < 0) fft_data[i] = 0;
				fft_data[i+1] = 0; //imag. part
			}

			//do FFT
			FFTmas10(fft_data-1, 128); //WARNING here, it's ok...

			/* #3. Magnitude conjugate. */
			for(i=0; i<128; i++){
				//spectrum_data[spectrum_last_idx][i] = fft_data[i<<1]*fft_data[i<<1] + fft_data[(i<<1)+1]*fft_data[(i<<1)+1];
				spectrum_data[spectrum_last_idx][i] = fft_data[i*2]*fft_data[i*2] + fft_data[(i*2)+1]*fft_data[(i*2)+1];
			}

			/* #4. Averaging, aka smoothing. */

			spectrum_last_idx++;
			if (spectrum_last_idx>=WINDOW_NUM){
				//wrap back to 0
				spectrum_last_idx = 0;
			}

			int tempIdx = 0; //signed
			float tempAverg;
			for(i=0; i<128; i++) {
				tempAverg = 0;
				for(j=1; j<=WINDOW_NUM; j++) {
					tempIdx = spectrum_last_idx - j;
					if (tempIdx < 0) {
						tempIdx = WINDOW_NUM + tempIdx;
					}
					tempAverg += spectrum_data[tempIdx][i];
				}
				spectrum_averg[i] = tempAverg/8;

				oled_equalizer_buf[i] = (u8)(((Xint16)spectrum_averg[i])>>RESCALING_FACTOR); //4

				//cap to 40
				if (oled_equalizer_buf[i] >= 40){
					oled_equalizer_buf[i] = 40;
				}
			}

			OLED_Clear();
			OLED_Equalizer_128_dotted(oled_equalizer_buf); //use DOT instead of BAR, to differentiate from main spectrum
		}

		stop_timer(timer_ctrl);
		//Calculate the time for the operation
		//xil_printf("Communication time %d us\n\r", (*timer_counter_l) / 333); // 1sec = 5 * 1000 * 1000 * 333 = able to hold by 4 bytes.
		tCount++;
	}

	/* ######## build the NOISE PROFILE for filter #################################################### */
	xil_printf("\nBuild the noise profile.\n");

	for ( i=0; i<128; i++ ) {
		if (spectrum_averg[i] > CAP_LEVEL) spectrum_averg[i] = CAP_LEVEL;
		spectrum_noise[i] = spectrum_averg[i];
	}

	//do smoothing over 9 points.
	for (j=0; j<2; j++) {
		//first 4 part
		spectrum_noise[0] = (spectrum_noise[4] + spectrum_noise[3] + spectrum_noise[2] + spectrum_noise[1] + spectrum_noise[0] + spectrum_noise[127] + spectrum_noise[126] + spectrum_noise[125] + spectrum_noise[124])/9;
		spectrum_noise[1] = (spectrum_noise[5] + spectrum_noise[4] + spectrum_noise[3] + spectrum_noise[2] + spectrum_noise[1] + spectrum_noise[0]   + spectrum_noise[127] + spectrum_noise[126] + spectrum_noise[125])/9;
		spectrum_noise[2] = (spectrum_noise[6] + spectrum_noise[5] + spectrum_noise[4] + spectrum_noise[3] + spectrum_noise[2] + spectrum_noise[1]   + spectrum_noise[0]   + spectrum_noise[127] + spectrum_noise[126])/9;
		spectrum_noise[3] = (spectrum_noise[7] + spectrum_noise[6] + spectrum_noise[5] + spectrum_noise[4] + spectrum_noise[3] + spectrum_noise[2]   + spectrum_noise[1]   + spectrum_noise[0]   + spectrum_noise[127])/9;
		//middle part
		for (i=4; i<124; i++) {
			spectrum_noise[i] = (spectrum_noise[i-4] + spectrum_noise[i-3] + spectrum_noise[i-2] + spectrum_noise[i-1] + spectrum_noise[i] + spectrum_noise[i+1]   + spectrum_noise[i+2]   + spectrum_noise[i+3]   + spectrum_noise[i+4])/9;
		}
		//last 4 part
		spectrum_noise[127] = (spectrum_noise[123] + spectrum_noise[124] + spectrum_noise[125] + spectrum_noise[126] + spectrum_noise[127] + spectrum_noise[0]   + spectrum_noise[1]   + spectrum_noise[2]   + spectrum_noise[3])/9;
		spectrum_noise[126] = (spectrum_noise[122] + spectrum_noise[123] + spectrum_noise[124] + spectrum_noise[125] + spectrum_noise[126] + spectrum_noise[127] + spectrum_noise[0]   + spectrum_noise[1]   + spectrum_noise[2])/9;
		spectrum_noise[125] = (spectrum_noise[121] + spectrum_noise[122] + spectrum_noise[123] + spectrum_noise[124] + spectrum_noise[125] + spectrum_noise[126] + spectrum_noise[127] + spectrum_noise[0]   + spectrum_noise[1])/9;
		spectrum_noise[124] = (spectrum_noise[120] + spectrum_noise[121] + spectrum_noise[122] + spectrum_noise[123] + spectrum_noise[124] + spectrum_noise[125] + spectrum_noise[126] + spectrum_noise[127] + spectrum_noise[0])/9;
	}

	//for(i=0;i<128;i++) {
	//	printf("\n%f", spectrum_noise[i]);
	//}

	//find the threshold
	float spectrumAverage, spectrumMinimum;
	Xint16 noiseThreshold;

	spectrumAverage = 0;
	spectrumMinimum = 0;
	for(i=0;i<128;i++) {
		spectrumAverage += spectrum_noise[i];
		if (spectrumMinimum > spectrum_noise[i]) {
			spectrumMinimum = spectrum_noise[i];
		}
	}
	spectrumAverage /= 128;
	//hence we get the noise threshold
	noiseThreshold = (Xint16)((spectrumAverage+spectrumMinimum)/2);
	printf("\nNoise Threshold = %d\n", noiseThreshold);

	printf("\n");



	/* ######## this is the main FREQUENCY ANALYZER part ############################################## */
	xil_printf("\nStarted...\n");

	/* in case .bss section is not really zeroed */
	spectrum_last_idx = 0;
	// clear spectrum_data[][] as well; this part is not counted for latency :)
	for(i=0;i<WINDOW_NUM;i++){
		for(j=0;j<128;j++){
			spectrum_data[i][j] = 0;
		}
	}

	/* go... */
	while(1)
	{
#if (MEASURE_TIME==1)
		init_timer(timer_ctrl, timer_counter_l, timer_counter_h);
		start_timer(timer_ctrl);
#endif

#if (SIMULATED_INPUT==0)
		get_audio(audio_data);
#else
		for(i=0; i<128; i++) {
			audio_data[i] = (Xint16)(5*sin(2*3.14*2*i/128)+5);//=SIN(2*PI()*2*B3/128)
		}
#endif


		//being paranoid
		for(i=0;i<WINDOW_NUM;i++){
			for(j=0;j<128;j++){
				spectrum_data[i][j] = 0;
			}
		}
		for(i=0; i<(128*2); i+=2){
			fft_data[i] = 0;
			fft_data[i+1] = 0; //imag. part
		}

		/* #1. Take 128 audio data -------------------------------------------------------------------- */
#if (DBG_AUDIO_DATA==1)
		/* debug */printf("\naudio_data\n");
#endif

		for(i=0;i<128;i++)
		{
			spectrum_data[spectrum_last_idx][i]=(audio_data[i])+10;
#if (DBG_AUDIO_DATA==1)
			/*debug*/printf("%d\n", (Xint16)spectrum_data[spectrum_last_idx][i]/*audio_data[i]*/);
#endif
		}

		/* #2. Do FFT, store in cyclic buffer of 8-size. ---------------------------------------------- */
		// construct for FFT data input
		for(i=0; i<(128*2); i+=2){
			fft_data[i] = spectrum_data[spectrum_last_idx][i]; //real part
			//only take positive value, why?
			//if (fft_data[i] < 0) fft_data[i] = 0;
			fft_data[i+1] = 0; //imag. part
		}

		//do FFT
		FFTmas10(fft_data-1, 128); //WARNING here, it's ok...

#if (DBG_FFT_OUT==1)
		/*debug*/printf("\nFFT result:\n");
#endif

		/* #2. Magnitude conjugate. ------------------------------------------------------------------- */
		for(i=0; i<128; i++){
			//spectrum_data[spectrum_last_idx][i] = fft_data[i<<1]*fft_data[i<<1] + fft_data[(i<<1)+1]*fft_data[(i<<1)+1];
			spectrum_data[spectrum_last_idx][i] = fft_data[i*2]*fft_data[i*2] + fft_data[(i*2)+1]*fft_data[(i*2)+1];
#if (DBG_FFT_OUT==1)
			/*debug*/printf("%f\n", spectrum_data[spectrum_last_idx][i]);
#endif
		}

		// not put noise cancellation here...

		/* #3. Averaging. ----------------------------------------------------------------------------- */

		spectrum_last_idx++;
		if (spectrum_last_idx>=WINDOW_NUM){
			//wrap back to 0
			spectrum_last_idx = 0;
		}

#if (DBG_SPECTRUM_AVERAGE==1)
		/*debug*/printf("spectrum_last_idx = %d\n", spectrum_last_idx);
		/*debug*/printf("average:\n");
#endif
		int tempIdx = 0; //signed
		float tempAverg;
		for(i=0; i<128; i++) {
			tempAverg = 0;
			for(j=1; j<=WINDOW_NUM; j++) {
				tempIdx = spectrum_last_idx - j;
				if (tempIdx < 0) {
					tempIdx = WINDOW_NUM + tempIdx;
				}
#if (DBG_SPECTRUM_AVERAGE==1)
				/*debug*/printf("[%d]",tempIdx);
#endif
				tempAverg += spectrum_data[tempIdx][i];
			}
			spectrum_averg[i] = tempAverg/8;
#if (DBG_SPECTRUM_AVERAGE==1)
			/*debug*/printf(" %f\n", spectrum_averg[i]);
#endif
		}

		/* #4. Noise cancelation ---------------------------------------------------------------------- */
		for(i=0; i<128; i++) {

			//cap first
			//spectrum_averg[i] *=2; //amplify
			if (spectrum_averg[i] > CAP_LEVEL) spectrum_averg[i] = CAP_LEVEL; //640

			//substract with noise profile
			//spectrum_averg[i] -= spectrum_noise[i];
			spectrum_averg[i] -= noiseThreshold;

			//threshold: 50; no need threshold since already taken care by Noise Profile substraction
			//spectrum_averg[i] -= TRESHOLD_LEVEL;


		/* #5. Display. ------------------------------------------------------------------------------- */

			//don't want negative
			if (spectrum_averg[i] <0) {
				spectrum_averg[i] = 0;
			}

			//fit into 40 height bar
			oled_equalizer_buf[i] = (u8)(((Xint16)spectrum_averg[i])>>RESCALING_FACTOR); //4

			//cap to 40
			if (oled_equalizer_buf[i] >= 40){
				oled_equalizer_buf[i] = 40;
			}

		}
		/* -------- */

#if (MEASURE_TIME==1)
		stop_timer(timer_ctrl);
		//Calculate the time for the operation
		xil_printf("Communication time %d us\n\r", (*timer_counter_l) / 333);
#endif

		OLED_Clear();
		OLED_Equalizer_128(oled_equalizer_buf);

	}
    return 0;
}
