/* FFT taken from Numerical Recipe in C */
/* Then we optimize the sin() function to predefined value. */
#include <stdio.h>

/* So that we know which part of code is optimized. */
#define OPTIMIZE_SIN 1

#if (OPTIMIZE_SIN==0)
/* NOTE: to use math.h, please also include "m" in gcc linker Library option. */
#include <math.h>
#endif

#define SWAP(a,b) tempr=(a);(a)=(b);(b)=tempr

#if (OPTIMIZE_SIN==1)
void FFTmas10(float data[], unsigned long nn)
#else
void FFTmas10(float data[], unsigned long nn)
#endif
{
	unsigned long n, mmax, m, j, istep, i;
	float/*double*/ wtemp, wr, wpr, wpi, wi;
#if (OPTIMIZE_SIN==0)
	float theta;
	int isign = -1; //T-to-F only, hope the compiler with optimize this.
#endif
	float tempr, tempi;

#if (OPTIMIZE_SIN==1)
	int idxHelper;
	float sin_theta[7] = {		-0, /* 2*PI/2 */
					-1, /* 2*PI/4 */
					-0.707106781186547, /* 2*PI/8 */
					-0.382683432365090, /* 2*PI/16 */
					-0.195090322016128, /* 2*PI/32 */
					-0.0980171403295606, /* 2*PI/64 */
					-0.0490676743274180  /* 2*PI/128 */ };
	/* note: sin(-x) = -sin(x) */
	float sin_half_theta[7] = {	-1, /* 2*PI/2/2 */
					-0.707106781186547, /* 2*PI/2/4 */
					-0.382683432365090, /* 2*PI/2/8 */
					-0.195090322016128, /* 2*PI/2/16 */
					-0.0980171403295606, /* 2*PI/2/32 */
					-0.0490676743274180, /* 2*PI/2/64 */
					-0.0245412285229123  /* 2*PI/2/128 */ };
#endif

	n=nn<<1;
	j=1;
	for(i=1;i<n;i+=2) /* asume array offset is 1, not 0 */
	{
		if (j>i)
		{
			SWAP(data[j],data[i]);
			SWAP(data[j+1],data[i+1]);
		}
		m=nn;
		while( m>=2 && j>m )
		{
			j -= m;
			m >>= 1;
		}
		j+=m;
	}

	/* Danielson-Lanczos */
	mmax=2;
#if(OPTIMIZE_SIN==1)
	idxHelper=0;
#endif
	while(n>mmax)
	{
		istep=mmax<<1;
#if (OPTIMIZE_SIN==1)
		//
		if (idxHelper > 7){
			printf("ERROR in FFT.");
			while(1);
		}
		wtemp = sin_half_theta[idxHelper];
#else
		theta=isign*(6.28318530717959/mmax);
		wtemp=sin(0.5*theta);
#endif
		wpr = -2.0*wtemp*wtemp;
#if (OPTIMIZE_SIN==1)
		//
		wpi = sin_theta[idxHelper];
		idxHelper++;
#else
		wpi=sin(theta);
#endif
		wr=1.0;
		wi=0.0;
		for(m=1;m<mmax;m+=2)
		{
			for(i=m;i<=n;i+=istep)
			{
				j=i+mmax;
				tempr=wr*data[j]-wi*data[j+1];
				tempi=wr*data[j+1]+wi*data[j];
				data[j]=data[i]-tempr;
				data[j+1]=data[i+1]-tempi;
				data[i] += tempr;
				data[i+1] += tempi;
			}
			wr=(wtemp=wr)*wpr-wi*wpi+wr;
			wi=wi*wpr+wtemp*wpi+wi;
		}
		mmax=istep;
	}

}
