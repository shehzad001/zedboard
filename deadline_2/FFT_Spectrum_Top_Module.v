`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:21:06 10/18/2014 
// Design Name: 
// Module Name:    FFT_Spectrum_Top_Module 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FFT_Spectrum_Top_Module(
	input aclk,
	input aresetn,
	
	//FFT inputs
	input [15 : 0] s_axis_config_tdata,
	input s_axis_config_tvalid,
	output s_axis_config_tready,
	input [63 : 0] s_axis_data_tdata,
	input s_axis_data_tvalid,
	output s_axis_data_tready,
	input s_axis_data_tlast,

	output [63 : 0] m_axis_data_tdata, // delete when done!
	output m_axis_data_tvalid,
	input m_axis_data_tready,
	output m_axis_data_tlast,
	
//Inputs contrl signals for Multiplier Core
	input Re_tvalid,
	input Im_tvalid,
	output Re_tready,
	output Im_tready,
	input Sq_tready,
	output Sq_tvalid,

//Inputs for Addition Core
	output Sq_Re_Im_tready,
	output Add_tvalid,
	input Add_tready,
	output Average_Data_tlast,
	input Average_tready,
	output [31:0] Memory_Values,
	output [31:0] Average_Data
    );

/*FFT Module Signals*/

	wire s_axis_config_tready1;
	wire s_axis_data_tready1;
	wire m_axis_data_tvalid1;
	wire m_axis_data_tlast1;
	wire [63 : 0] m_axis_data_tdata1;

//For the real and imaginary input data signals
	wire [31:0] Re_tdata;
	wire [31:0] Im_tdata;

//----------- INSTANTIATION Template ---// INST_TAG
FFTCore_TopModule FFTCore_Top(
  .aclk(aclk), // input aclk
  .s_axis_config_tdata(s_axis_config_tdata), // input [15 : 0] s_axis_config_tdata
  .s_axis_config_tvalid(s_axis_config_tvalid), // input s_axis_config_tvalid
  .s_axis_config_tready(s_axis_config_tready1), // output s_axis_config_tready
  .s_axis_data_tdata(s_axis_data_tdata), // input [63 : 0] s_axis_data_tdata
  .s_axis_data_tvalid(s_axis_data_tvalid), // input s_axis_data_tvalid
  .s_axis_data_tready(s_axis_data_tready1), // output s_axis_data_tready
  .s_axis_data_tlast(s_axis_data_tlast), // input s_axis_data_tlast
  .m_axis_data_tdata(m_axis_data_tdata1), // output [63 : 0] m_axis_data_tdata  ######  // delete when done!
  .m_axis_data_tvalid(m_axis_data_tvalid1), // output m_axis_data_tvalid
  .m_axis_data_tready(m_axis_data_tready), // input m_axis_data_tready
  .m_axis_data_tlast(m_axis_data_tlast1), // output m_axis_data_tlast
  .event_frame_started(), // output event_frame_started
  .event_tlast_unexpected(), // output event_tlast_unexpected
  .event_tlast_missing(), // output event_tlast_missing
  .event_status_channel_halt(), // output event_status_channel_halt
  .event_data_in_channel_halt(), // output event_data_in_channel_halt
  .event_data_out_channel_halt(), // output event_data_out_channel_halt
  .Re_out(Re_tdata),
  .Im_out(Im_tdata)
);

	assign s_axis_config_tready = s_axis_config_tready1;
	assign s_axis_data_tready = s_axis_data_tready1;
	assign m_axis_data_tvalid = m_axis_data_tvalid1;
	assign m_axis_data_tlast = m_axis_data_tlast1;
	assign m_axis_data_tdata = m_axis_data_tdata1;


/*Magnitude Module Signals*/

	wire Re_tready1;
	wire Im_tready1;
	wire Sq_tvalid1;
	wire Sq_Re_Im_tready1;
	wire Add_tvalid1;
	wire [31:0] Ab_Mag_tdata_in;

	Absolute_Magnitude Absolute_Magnitude_Top (
		.aclk(aclk), // input aclk,
		.aresetn(aresetn), // input aresetn,
		.Re_tdata(Re_tdata), // input [31:0] Re_tdata,
		.Im_tdata(Im_tdata), // input [31:0] Im_tdata,
		.Re_tvalid(Re_tvalid), // input Re_tvalid,
		.Im_tvalid(Im_tvalid), // input Im_tvalid,
		.Re_tready(Re_tready1), // output Re_tready,
		.Im_tready(Im_tready1), // output Im_tready,
		.Sq_tready(Sq_tready), // input Sq_tready,
		.Sq_tvalid(Sq_tvalid1), // output Sq_tvalid,
		.Sq_Re_Im_tready(Sq_Re_Im_tready1), //output Sq_Re_Im_tready,
		.Add_tvalid(Add_tvalid1), // output Add_tvalid,
		.Add_tready(Add_tready), // input Add_tready,
		.Ab_Mag_tdata(Ab_Mag_tdata_in) // output [31:0] Ab_Mag_tdata
	);

	assign Re_tready = Re_tready1;
	assign Im_tready = Im_tready1;
	assign Sq_tvalid = Sq_tvalid1;
	assign Sq_Re_Im_tready = Sq_Re_Im_tready1;
	assign Add_tvalid = Add_tvalid1;

/*Averaging Module Signals*/

	wire start_siso_data_in_top;
	assign start_siso_data_in_top = Add_tvalid && Add_tready;

	AveragingModule Averaging(
		.aclk(aclk), 
		.aresetn(aresetn), 
		.siso_out(Memory_Values), 
		.siso_in(Ab_Mag_tdata_in), 
		.start_siso_data_in(start_siso_data_in_top), 
		.siso_valid_data_out(Average_tready),
		.siso_valid(Average_Data_tlast),
		.Mag_Add_Reg(Average_Data)
	);

endmodule
