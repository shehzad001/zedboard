`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   01:17:35 10/15/2014
// Design Name:   DivFloat
// Module Name:   D:/NTU/ZedBoard/demo/ISE_project/FFT/FFT/DivFloat_tb.v
// Project Name:  FFT
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: DivFloat
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module DivFloat_tb;

	// Inputs
	reg aclk;
	reg aresetn;
	reg s_axis_a_tvalid;
	reg s_axis_b_tvalid;
	reg m_axis_result_tready;
	reg [31:0] s_axis_a_tdata;
	reg [31:0] s_axis_b_tdata;
	reg [4:0] i;

	// Outputs
	wire s_axis_a_tready;
	wire s_axis_b_tready;
	wire m_axis_result_tvalid;
	wire [31:0] m_axis_result_tdata;

	// Instantiate the Unit Under Test (UUT)
	DivFloat uut (
		.aclk(aclk), 
		.aresetn(aresetn), 
		.s_axis_a_tvalid(s_axis_a_tvalid), 
		.s_axis_b_tvalid(s_axis_b_tvalid), 
		.m_axis_result_tready(m_axis_result_tready), 
		.s_axis_a_tready(s_axis_a_tready), 
		.s_axis_b_tready(s_axis_b_tready), 
		.m_axis_result_tvalid(m_axis_result_tvalid), 
		.s_axis_a_tdata(s_axis_a_tdata), 
		.s_axis_b_tdata(s_axis_b_tdata), 
		.m_axis_result_tdata(m_axis_result_tdata)
	);

	initial begin
		aclk = 0;
		forever #5 aclk = !aclk;
	end

	initial begin
		// Initialize Inputs
		aclk = 0;
		aresetn = 0;
		s_axis_a_tvalid = 0;
		s_axis_b_tvalid = 0;
		m_axis_result_tready = 0;
		s_axis_a_tdata = 0;
		s_axis_b_tdata = 0;

		// Wait 100 ns for global reset to finish
		#50;
	
		// Add stimulus here

		@(posedge aclk); aresetn = 0;
		@(posedge aclk); @(posedge aclk); aresetn = 1;

		repeat(10) begin
			@(posedge aclk); 
		end

		while ((s_axis_a_tready == 0) && (s_axis_b_tready == 0)) begin
			@(posedge aclk);
		end

		s_axis_a_tvalid = 1; //set valid
		s_axis_b_tvalid = 1; //set valid
		for(i=0; i<10; i=i+1) begin //9 should be feasible !!!
			if(i==0) begin
				s_axis_a_tdata = 32'b01000010100000000000000000000000; //64
				s_axis_b_tdata = 32'b01000010100000000000000000000000;
			end
			else if(i==1) begin
				s_axis_a_tdata = 32'b01000010000000000000000000000000; //32
				s_axis_b_tdata = 32'b01000010000000000000000000000000;
			end
			else if(i==2) begin
				s_axis_a_tdata = 32'b01000001100000000000000000000000; //16
				s_axis_b_tdata = 32'b01000001100000000000000000000000;
			end
			else if(i==3) begin
				s_axis_a_tdata = 32'b01000001000000000000000000000000; //8
				s_axis_b_tdata = 32'b01000001000000000000000000000000;
			end
			else if(i==4) begin
				s_axis_a_tdata = 32'b01000000100000000000000000000000; //4
				s_axis_b_tdata = 32'b01000000100000000000000000000000;
			end
			else begin
				s_axis_a_tdata = 32'b01000000000000000000000000000000; //2
				s_axis_b_tdata = 32'b01000000000000000000000000000000;
			end 
			$display("\n\tInput Data A:");
			$monitor("\t%0x\n",s_axis_a_tdata);
			$display("\n\tInput Data B:");
			$monitor("\t%0x\n",s_axis_b_tdata);
			@(posedge aclk);
		end

		s_axis_a_tvalid = 0; //reset tvalid pins
		s_axis_b_tvalid = 0; //reset tvalid pins
		
		while (m_axis_result_tvalid == 0) @(posedge aclk); //polling

		m_axis_result_tready = 1; //set valid for output 

		while (m_axis_result_tvalid == 1) begin
				$display("\t\tOutput Data after Division");
				$monitor("\t\t''%b",m_axis_result_tdata);
				@(posedge aclk);
		end




		/*** After taking 10 Output Data ***/
		while ((s_axis_a_tready == 0) && (s_axis_b_tready == 0)) begin
			@(posedge aclk);
		end

		s_axis_a_tvalid = 1; //set valid
		s_axis_b_tvalid = 1; //set valid
		for(; i<21; i=i+1) begin
			s_axis_a_tdata = 32'b01000000100000000000000000000000; //4
			s_axis_b_tdata = 32'b01000000100000000000000000000000;
			$display("\n\tInput Data A:");
			$monitor("\t%0x\n",s_axis_a_tdata);
			$display("\n\tInput Data B:");
			$monitor("\t%0x\n",s_axis_b_tdata);
			@(posedge aclk);
		end

		s_axis_a_tvalid = 0; //reset tvalid pins
		s_axis_b_tvalid = 0; //reset tvalid pins
		
		while (m_axis_result_tvalid == 0) @(posedge aclk); //polling

		m_axis_result_tready = 1; //set valid for output 

		while (m_axis_result_tvalid == 1) begin
				$display("\t\tOutput Data after Division");
				$monitor("\t\t''%b",m_axis_result_tdata);
				@(posedge aclk);
		end

		@(posedge aclk); @(posedge aclk); @(posedge aclk); @(posedge aclk); $finish;
	end      
      
endmodule
