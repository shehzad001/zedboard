`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:39:24 10/14/2014
// Design Name:   AveragingModule
// Module Name:   D:/NTU/ZedBoard/demo/ISE_project/FFT/FFT/AveragingModule_tb.v
// Project Name:  FFT
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: AveragingModule
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module AveragingModule_tb;

	// Inputs
	reg aclk;
	reg aresetn;
	reg [31:0] siso_in;
	reg start_siso_data_in;
	reg siso_valid_data_out;
	reg [7:0] i;

	// Outputs
	wire [31:0] siso_out;
	wire siso_valid;
	wire [31:0] Mag_Add_Reg;

	// Instantiate the Unit Under Test (UUT)
	AveragingModule uut (
		.aclk(aclk), 
		.aresetn(aresetn), 
		.siso_out(siso_out), 
		.siso_in(siso_in), 
		.start_siso_data_in(start_siso_data_in), 
		.siso_valid_data_out(siso_valid_data_out),
		.siso_valid(siso_valid),
		.Mag_Add_Reg(Mag_Add_Reg)
	);

	initial begin
		aclk = 0;
		forever #50 aclk = !aclk;
	end

	parameter max_data_value = 8'd3;
	parameter max_data_value_less_than_one = max_data_value - 1'b1;

	initial begin
		// Initialize Inputs
		aclk = 0;
		aresetn = 1;
		siso_in = 0;
		start_siso_data_in = 0;
		siso_valid_data_out = 0;

		// Wait 100 ns for global aresetn to finish
		#50;
        
		// Add stimulus here
		
		@(posedge aclk); aresetn = 0;
		@(posedge aclk); aresetn = 1;
		
		@(posedge aclk); @(posedge aclk); @(posedge aclk); 

		repeat(7) begin
			start_siso_data_in = 1; //set valid
			for(i=0; i<max_data_value; i=i+1) begin
				if(i==0)
					siso_in = 32'b01000000100000000000000000000000; //4
				else if(i<max_data_value_less_than_one)
					siso_in = 32'b01000010100000000000000000000000; //64
				else
					siso_in = 32'b01000000000000000000000000000000; //2
				@(posedge aclk);
			end
			
			start_siso_data_in = 0; //reset valid
			@(posedge aclk);

			for(i=0; i<max_data_value; i=i+1) begin
				siso_valid_data_out = 1;
				$display("\n\tOutput Data:");
				$monitor("\t%0x\n",siso_out);
				@(posedge aclk);
			end

			siso_valid_data_out = 0;
			repeat(1) begin
				@(posedge aclk);
			end
		end
		

		start_siso_data_in = 1; //set valid
		for(i=0; i<max_data_value; i=i+1) begin
		// Same Data as above.
			if(i==0)
				siso_in = 32'b01000000100000000000000000000000; //4
			else if(i<max_data_value_less_than_one)
				siso_in = 32'b01000010100000000000000000000000; //64
			else
				siso_in = 32'b01000000000000000000000000000000; //2
			@(posedge aclk);
		// Different Data from above.
/*			if(i==0)
				siso_in = 32'b01000010100000000000000000000000; //64
			else if(i<max_data_value_less_than_one)
				siso_in = 32'b01000000000000000000000000000000; //2
			else
				siso_in = 32'b01000000100000000000000000000000; //4
			@(posedge aclk);
*/		end
		
		start_siso_data_in = 0; //reset valid
		@(posedge aclk);

		repeat(4) begin
			$display("\n\tMagnitude Addition Data:");
			$monitor("\t''%b\n",Mag_Add_Reg);
			@(posedge aclk);
		end

/*		for(i=0; i<max_data_value; i=i+1) begin
			siso_valid_data_out = 1;
			$display("\n\tOutput Data:");
			$monitor("\t%0x\n",siso_out);
			@(posedge aclk);
		end

		siso_valid_data_out = 0;
		repeat(5) begin
			@(posedge aclk);
		end

		
		repeat(2) begin
			@(posedge aclk); 
		end
*/		
		@(posedge aclk); @(posedge aclk); $finish;
	end
      
endmodule
