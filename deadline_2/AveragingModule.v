`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:46:51 10/14/2014 
// Design Name: 
// Module Name:    AveragingModule 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module AveragingModule(
	input aclk,
	input aresetn,

	//Siso
	output reg [31:0] siso_out,
	input [31:0] siso_in,
	input start_siso_data_in,
	input siso_valid_data_out,
	output siso_valid,

	//Add Block
	output reg [31:0] Mag_Add_Reg
	);
	
	reg [31:0] siso [0:7][0:127]; // Array size = [8][128]
	reg [6:0] address_in; // 8-bit counter, 1-bit extra for the counter to stop at 128
	reg [6:0] address_out; // 8-bit counter, 1-bit extra for the counter to stop at 128
	reg [3:0] Win_No; // 4-bit for 8 Windows
	
	parameter reset_count = 7'd0;
	parameter count_max_value = 7'd127;
	
	wire last_data_out;

	// Memory Block
	always @(posedge aclk)
		if(!aresetn)//synchronous reset
			siso[Win_No][address_in] <=	32'd0;
		else if (start_siso_data_in && !stop_utill_add)
			siso[Win_No][address_in] <=	siso_in;

	// Input_Counter !
	always @(posedge aclk)
		if(!aresetn)//synchronous reset
			address_in <=	reset_count;
		else if (start_siso_data_in && (address_in < count_max_value) && !stop_utill_add)
			address_in <=	address_in + 1'b1;
		else if (start_siso_data_in && siso_valid) // Reset = Data Stops + Last Data
			address_in <=	reset_count;

	always @(posedge aclk)
		if(!aresetn)//synchronous reset
			siso_out <=	32'd0;
		else if (siso_valid_data_out)// && !stop_utill_add)
			siso_out	<=	siso[win_out][address_out];

	// Output_Counter !
	always @(posedge aclk)
		if(!aresetn)//synchronous reset
			address_out <=	reset_count;
		else if (siso_valid_data_out && (address_out < count_max_value))// && !stop_utill_add)
			address_out <=	address_out + 1'b1;
		else if (siso_valid_data_out && last_data_out) // Reset = Data Stops + Last Data
			address_out <=	reset_count;

	//making a output window variable
	
	reg [3:0] win_out; // 4-bit for 8 Windows

	wire Reset_frame_out;
	assign Reset_frame_out = (win_out == 4'd8) ? 1'b1 : 1'b0;

	always @(posedge aclk)
		if(!aresetn)//synchronous reset
			win_out <= 4'd0;
		else if(Reset_frame_out)
			win_out <= 4'd0; // resets after 1 clk i.e. 8 --> 0
		else if (last_data_out && (win_out < 4'd8))
			win_out <= win_out + 1'b1; //till 8
	
	//Window Counter
	assign siso_valid = (address_in == count_max_value) ? 1'b1 : 1'b0;
	assign last_data_out = (address_out == count_max_value) ? 1'b1 : 1'b0;

	wire Reset_frame;
	assign Reset_frame = (Win_No == 4'd8) ? 1'b1 : 1'b0;
	
	always @(posedge aclk)
		if(!aresetn)//synchronous reset
			Win_No <= 4'd0;
		else if(Reset_frame)
			Win_No <= 4'd0; // resets after 1 clk i.e. 8 --> 0
		else if (siso_valid && (Win_No < 4'd8))
			Win_No <= Win_No + 1'b1; //till 8
	
	wire s_axis_a_tready_top;
	wire s_axis_b_tready_top;
	wire Mag_Add_tready = s_axis_a_tready_top && s_axis_b_tready_top;

	reg [7:0] Row_count;	// Array size = [8][3]
	parameter Row_max_lenght = 2'd3;
	parameter Row_reset = 2'b0;
	reg [2:0] Col_count; // first input is 2 therefore till 7, makes total 8 windows
	parameter Win_lenght = 3'd7; // taking 8-1 = 7 for above cond
	parameter Col_reset = 3'b0;
	reg Add_aclk;
	reg Mag_Add_tvalid;
	reg [31:0] Mag_A_in;
	reg [31:0] Mag_B_in;
	wire Mag_Add_result_Flag;
	reg Mag_Add_result_tready;
	wire [31:0] w_temp_add_result;
	reg stop_utill_add;
	reg start_add;
	
	always@(posedge aclk)
		if(!aresetn) begin
			start_add <= 0;
			stop_utill_add <= 0; // !stop_utill_add means zero(0)
		end
		else if(Reset_frame) begin
			start_add <= 1;
			stop_utill_add <= 1;
		end
		else if(Row_count == (Row_max_lenght - 1'b1)) begin
			start_add <= 0;
			stop_utill_add <= 0;
		end

	always@(posedge aclk)
		if(!aresetn) begin
			Row_count <= Row_reset;
			Col_count <= Col_reset;
			Add_aclk <= 0;
			Mag_Add_tvalid <= 0;
		end
		else if(start_add && stop_utill_add) begin // window is reset and AddCore is ready to accept the input
			for(Row_count = 0; Row_count<Row_max_lenght; Row_count = Row_count + 1'b1) begin 
				for(Col_count = 0; Col_count<Win_lenght; Col_count = Col_count + 1'b1) begin
					if(Mag_Add_tready) begin
						if(Col_count == 1'b0) begin
							Mag_A_in <= siso[Col_count][Row_count]; // [0][0]
							Mag_B_in <= siso[Col_count + 1'b1][Row_count]; // [0][0]
							Mag_Add_tvalid <= 1;
						end
						else begin //if(Mag_Add_result_Flag) begin
							Mag_A_in <= w_temp_add_result; // [0][0]
							Mag_B_in <= siso[Col_count + 1'b1][Row_count]; // [0][0]
							Mag_Add_tvalid <= 1;
						end
						#1 Add_aclk <= !Add_aclk; //posedge [0 --> 1]
						#1 Add_aclk <= !Add_aclk; //negedge [1 --> 0]
						while(!Mag_Add_result_Flag) begin // wait for the output
							#1 Add_aclk <= !Add_aclk; //posedge [0 --> 1]
							#1 Add_aclk <= !Add_aclk; //negedge [1 --> 0]
							Mag_Add_tvalid <= 0;
						end
						while(Mag_Add_result_Flag) begin // wait to fetch output
							#1 Add_aclk <= !Add_aclk; //posedge [0 --> 1]
							#1 Add_aclk <= !Add_aclk; //negedge [1 --> 0]
						end
					end
				end
			end
		end

//	reg [31:0] Add_A_in;
//	reg [31:0] Add_B_in;
//	parameter [31:0] Add_B_in = 32'b01000001000000000000000000000000; // Constant Value = 8

	always@(posedge Add_aclk)
		if(!aresetn) begin
			Mag_Add_result_tready = 0; // output result contrl signal
//			Mag_Add_Reg = 32'b0;
		end
		else if(Mag_Add_result_Flag) begin
			Mag_Add_result_tready <= 1; //Fetch the output
			if(Col_count == Win_lenght - 1'b1) begin
				Mag_Add_Reg <= w_temp_add_result;
//				Add_A_in <= w_temp_add_result;
//				Add_B_in <= 32'b01000001000000000000000000000000; // Constant Value = 8
			end
		end
		else if(!Mag_Add_result_Flag)
			Mag_Add_result_tready <= 0; //Core have no data, make it low
	

// Memory of 128 to Store the Data Set of Average values!

/*	reg [31:0] Mag_Add_Reg [0:2] // Array size = [3] --> // Array size = [128]
	reg [1:0] Avg_Count; // 8-bit counter, 1-bit extra for the counter to stop at 128

	always @(posedge Add_aclk)
		if(!aresetn)//synchronous reset
			Mag_Add_Result <=	32'd0;
		else if (Col_count == Win_lenght - 1'b1)
			Mag_Add_Result	<=	Mag_Add_Reg[Avg_Count];

	// Output_Counter !
	always @(posedge Add_aclk)
		if(!aresetn)//synchronous reset
			Avg_Count <=	reset_count;
		else if (siso_valid_data_out && (Avg_Count < count_max_value))
			Avg_Count <=	Avg_Count + 1'b1;
		else if (siso_valid_data_out && last_data_out) // Reset = Data Stops + Last Data
			Avg_Count <=	reset_count;
*/

	AddFloat Mag_Add_Frame(
	  .aclk(Add_aclk), // input aclk
	  .aresetn(aresetn), // input aresetn
	  .s_axis_a_tvalid(Mag_Add_tvalid), // input s_axis_a_tvalid
	  .s_axis_a_tready(s_axis_a_tready_top), // output s_axis_a_tready
	  .s_axis_a_tdata(Mag_A_in), // input [31 : 0] s_axis_a_tdata
	  .s_axis_b_tvalid(Mag_Add_tvalid), // input s_axis_b_tvalid
	  .s_axis_b_tready(s_axis_b_tready_top), // output s_axis_b_tready
	  .s_axis_b_tdata(Mag_B_in), // input [31 : 0] s_axis_b_tdata
	  .m_axis_result_tvalid(Mag_Add_result_Flag), // output m_axis_result_tvalid
	  .m_axis_result_tready(Mag_Add_result_tready), // input m_axis_result_tready
	  .m_axis_result_tdata(w_temp_add_result) // output [31 : 0] m_axis_result_tdata
	);
	
/*	wire s_axis_a_tready_top_a;
	wire s_axis_b_tready_top_b;
	wire Div_in_tready = s_axis_a_tready_top_a && s_axis_b_tready_top_b;
	
	reg Div_in_tvalid;
	always @(posedge Add_aclk)
		if(!aresetn)
			Div_in_tvalid <= 0;
		else if((Col_count == Win_lenght - 1'b1) && Div_in_tready)
			Div_in_tvalid <= 1;
		else Div_in_tvalid <= 0;
			
//	assign Div_in_tvalid = ((Col_count == Win_lenght - 1'b1) && Div_in_tready) ? 1'b1 : 1'b0;

	reg Div_result_tready;	
	always @(posedge Add_aclk)
		if(!aresetn)
			Div_result_tready = 0; // output result contrl signal for Div
		else if(Div_result_tvalid)
			Div_result_tready <= 1; //Fetch the output
		else if(!Div_result_tvalid)
			Div_result_tready <= 0; //Core have no data, make it low

	DivFloat Avg_Block(
	  .aclk(Add_aclk), // input aclk
	  .aresetn(aresetn), // input aresetn
	  .s_axis_a_tvalid(Div_in_tvalid), // input s_axis_a_tvalid
	  .s_axis_a_tready(s_axis_a_tready_top_a), // output s_axis_a_tready
	  .s_axis_a_tdata(Add_A_in), // input [31 : 0] s_axis_a_tdata
	  .s_axis_b_tvalid(Div_in_tvalid), // input s_axis_b_tvalid
	  .s_axis_b_tready(s_axis_b_tready_top_b), // output s_axis_b_tready
	  .s_axis_b_tdata(Add_B_in), // input [31 : 0] s_axis_b_tdata
	  .m_axis_result_tvalid(Div_result_tvalid), // output m_axis_result_tvalid
	  .m_axis_result_tready(Div_result_tready), // input m_axis_result_tready
	  .m_axis_result_tdata(Mag_Add_Reg) // output [31 : 0] m_axis_result_tdata
	);
*/

endmodule
